<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>
<?php
    $name = $surname = $choose = "";
    $nameErr = $surnameErr = $chooseErr = "";
    
    if (isset($_GET['name'])) { $name = $_GET['name']; }
    if (isset($_GET['surname'])) { $surname = $_GET['surname']; }
    if (isset($_GET['choose'])) { $choose = $_GET['choose']; }

    if (isset($_GET['nameErr'])) { $nameErr = $_GET['nameErr']; }
    if (isset($_GET['surnameErr'])) { $surnameErr = $_GET['surnameErr']; }
    if (isset($_GET['chooseErr'])) { $chooseErr = $_GET['chooseErr']; } 
?>
    
    <form method="post" name="upload" action="myPhoto.php" enctype="multipart/form-data" >
        Name: <input type="text" name="name" value="<?php echo $name;?>">
        <span class="error">* <?php echo $nameErr;?></span>
        <br><br>

        Surname: <input type="text" name="surname" value="<?php echo $surname;?>">
        <span class="error">* <?php echo $surnameErr;?></span>
        <br><br>

        Choose:
        <input type="radio" name="choose" <?php if (isset($choose) && $choose=="public") echo "checked";?> value="public">Public
        <input type="radio" name="choose" <?php if (isset($choose) && $choose=="private") echo "checked";?> value="private">Private
        <span class="error">* <?php echo $chooseErr;?></span>
        <br><br> 

        <label for="if">File:</label>
        <input type="file" name="photo" id="if"/><br /><br />
        <input type="submit" name="sb" id="sb" value="upload" />
        <input type="reset" name="rb" id="rb" value="cancel" />
    </form> 
</body>
</html>